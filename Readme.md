# Crush that castle! #
This is third delivery from Expert Development Videogame grade taught in the Faculty of Computer Science, Ciudad Real.

This game works with:

* Ogre3D like Rendering Engine.
* OIS like Event Listener.
* Bullet like Physics Engine.
* SDL like sound manager.