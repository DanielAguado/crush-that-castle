#include "game.h"

Play::Play(Game::shared game): State(game) {
  _init = false;
  _session = std::make_shared<Session>(game);
}

Play::~Play() {
}

void
Play::init() {
  if(!_init){
    _session->initialize(_game->_player);
    _init = true;
  }
  else{
    CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
    context_.setRootWindow(_session->_ball->_power_bar);
  }
  
  if(_session->_next_scenario)
    _session->next_scenario();
  
  add_callbacks();
}

void
Play::exit() {
  _game->_input->clear_hooks();
  _game->_sound->pause_music();
}

void
Play::pause() {
  exit();
  State::_game->set_current_state("pause");
}

void
Play::end_game() {
  _init = false;
  exit();

  if(_game->has_ended()){
    _session->reset_overlays();
    State::_game->set_current_state("results");
  }
}

void
Play::add_callbacks() {
  _game->_input->add_hook({OIS::KC_ESCAPE, EventTrigger::OnKeyPressed},
                          EventType::doItOnce,
                          std::bind(&Play::pause, this));
  _session->register_keybindings();
}


void
Play::resume() {
}

void
Play::update() {
  _session->update(_game->_delta);

  if(_session->_finished){
    State::_game->set_current_state("results");
    _session->_finished = false;
  }
}
