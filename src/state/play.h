#ifndef PLAY_H
#define PLAY_H
#include "state.h"
#include "session.h"
#include "camera_slider.h"

class Play : public State {
  bool _win, _init;
public:
  Play(std::shared_ptr<Game> game);
  virtual ~Play();

  virtual void init();
  virtual void resume();
  virtual void exit();
  virtual void update();

  virtual void pause();
  virtual void end_game();

 private:
  Session::shared _session;

  void add_callbacks();

  void move_camera(Ogre::Vector3 increment);
};

#endif
