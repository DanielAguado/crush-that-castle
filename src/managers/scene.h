#ifndef SCENE_H
#define SCENE_H
#include <OgreRoot.h>
#include <OgreLogManager.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>
#include <OgreCamera.h>
#include <OgreSceneManager.h>
#include <OgreResourceGroupManager.h>
#include <OgreConfigFile.h>
#include <OgreManualObject.h>
#include <OgreEntity.h>
#include <OgreMeshManager.h>
#include <OgreParticleSystem.h>
#include "OgreBillboardSet.h"

#include <LinearMath/btVector3.h>

#include <memory>

class Scene {

  Ogre::Root* _root;
  Ogre::SceneManager* _scene_manager;
  std::vector<Ogre::Camera*> _camera_list;

 public:
  typedef std::shared_ptr<Scene> shared;

  Ogre::RenderWindow* _window;
  Ogre::Camera* _camera;
  Ogre::RaySceneQuery* _ray_query;

  Scene();
  void render_one_frame(void);
  Ogre::Ray set_ray_query(float x, float y);
  void create_light(void);
  int create_camera(std::string name, Ogre::Vector3 position, Ogre::Vector3 lookAt);
  void switch_camera(int index);

  Ogre::SceneNode* get_node(std::string node);
  void attach(Ogre::SceneNode* node, Ogre::MovableObject* entity);

  Ogre::SceneNode* create_graphic_element(std::string entity,
        std::string mesh, std::string parent, std::string name);
  Ogre::SceneNode* create_graphic_element(Ogre::Entity* entity,
        std::string parent, std::string name);

  Ogre::SceneNode* create_plane(std::string axis, std::string name, std::string mesh,
        std::string parent, std::string material);

  Ogre::SceneNode* get_child_node(std::string parent, std::string name);
  Ogre::SceneNode* get_child_node(Ogre::SceneNode* parent, std::string name);

  Ogre::Entity* create_entity(std::string name, std::string mesh, bool cast_shadows = true);

  void move_node(std::string node_name, Ogre::Vector3 increment);

  Ogre::ParticleSystem* create_particle(std::string node_name, std::string particle_system);
  Ogre::ParticleSystem* create_particle(Ogre::SceneNode* node, std::string particle_name, std::string particle_system);
  Ogre::ParticleSystem* create_particle(std::string particle_name, std::string particle_system, Ogre::Vector3 position);

  void create_billboard(std::string billboard_name,
                        std::string material_name,
                        std::vector<Ogre::Vector3> positions,
                        std::string node_name = "");

  void destroy_node(std::string);
  void destroy_node(Ogre::SceneNode* child);

  void remove_child(std::string parent, std::string child);
  void remove_child(std::string parent, Ogre::SceneNode* child);
  void remove_child(Ogre::SceneNode* parent, std::string child);
  void remove_child(Ogre::SceneNode* parent, Ogre::SceneNode* child);

  void destroy_scene();
  void destroy_all_attached_movable_objects(Ogre::SceneNode* node);

  Ogre::Vector3 convert_btvector3_to_vector3(btVector3 position);
  btVector3 convert_vector3_to_btvector3(Ogre::Vector3 position);

 private:
  void load_resources();
  Ogre::SceneNode* create_node(std::string name);
  Ogre::SceneNode* create_child_node(Ogre::SceneNode* parent, std::string name);
  void add_child(Ogre::SceneNode* parent, Ogre::SceneNode* child);

  Ogre::Vector3 get_axis(std::string axis);
  Ogre::Vector3 get_normal(std::string axis);
};


#endif
