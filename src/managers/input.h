// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#ifndef INPUT_HPP
#define INPUT_HPP
#include <map>
#include <vector>
#include <memory>
#include <unistd.h>

#include <OgreRoot.h>
#include <OgreRenderWindow.h>
#include <OgreWindowEventUtilities.h>

#include <CEGUI/CEGUI.h>

#include <OIS/OISEvents.h>
#include <OIS/OISInputManager.h>
#include <OIS/OISKeyboard.h>
#include <OIS/OISMouse.h>

enum class EventType{repeat, doItOnce};
enum class EventTrigger{ OnKeyPressed, OnKeyReleased};

class EventListener: public Ogre::WindowEventListener,
  public OIS::KeyListener,
  public OIS::MouseListener {

  typedef bool OnKeyPressed;
  typedef std::pair<OIS::MouseButtonID, EventTrigger> MouseKey;
  typedef std::pair<OIS::KeyCode, EventTrigger> KeyBoardKey;

  OIS::InputManager* _input_manager;
  OIS::Mouse* _mouse;
  OIS::Keyboard* _keyboard;

  MouseKey _mouse_key;

  std::map<MouseKey, std::function<void()>> _mouse_triggers;

 public:
  typedef std::shared_ptr<EventListener> shared;
  typedef std::vector<KeyBoardKey> KeyEvents;

  float _x, _y;
  bool _exit;

  EventListener(Ogre::RenderWindow* window);

  void capture(void);
  void check_events();

  void add_hook(MouseKey key,std::function<void()> callback);
  void add_hook(KeyBoardKey keystroke, EventType type, std::function<void()> callback);
  void clear_hooks();

  bool keyPressed(const OIS::KeyEvent& arg);
  bool keyReleased(const OIS::KeyEvent& arg);
  bool mouseMoved(const OIS::MouseEvent&  evt);
  bool mousePressed(const OIS::MouseEvent& evt, OIS::MouseButtonID id);
  bool mouseReleased(const OIS::MouseEvent& evt, OIS::MouseButtonID id);

  void windowClosed(Ogre::RenderWindow* window);

  bool shutdown(void);
  bool gui_shutdown(const CEGUI::EventArgs &event);

 private:
  KeyEvents _events;
  std::map<KeyBoardKey, std::function<void()>> _repeat_triggers, _doitonce_triggers;

  void create_input_manager(Ogre::RenderWindow* window);
  void remove_key_from_buffer(KeyBoardKey event);

  void trigger_keyboard_events();
  void trigger_mouse_events();

  CEGUI::MouseButton convertMouseButton(OIS::MouseButtonID id);
  };
#endif