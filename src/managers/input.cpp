// -*- coding:utf-8; tab-width:4; mode:cpp -*-
// Copyright (C) 2014  ISAAC LACOBA MOLINA
// Tinman author: Isaac Lacoba Molina
//
//     This program is free software: you can redistribute it and/or modify
//     it under the terms of the GNU General Public License as published by
//     the Free Software Foundation, either version 3 of the License, or
//     (at your option) any later version.
//
//     This program is distributed in the hope that it will be useful,
//     but WITHOUT ANY WARRANTY; without even the implied warranty of
//     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//     GNU General Public License for more details.
//
//     You should have received a copy of the GNU General Public License
//     along with this program.  If not, see <http://www.gnu.org/licenses/>.
//

#include "input.h"

EventListener::EventListener(Ogre::RenderWindow* window) {
  _x = _y = 0;
  _exit = false;
  _mouse_key.first = OIS::MB_Button7;

  create_input_manager(window);

  _keyboard = static_cast<OIS::Keyboard*>
    (_input_manager->createInputObject(OIS::OISKeyboard, true));
  _mouse = static_cast<OIS::Mouse*>
    (_input_manager->createInputObject(OIS::OISMouse, true));

  _keyboard->setEventCallback(this);
  _mouse->setEventCallback(this);
  Ogre::WindowEventUtilities::addWindowEventListener(window, this);
}

void
EventListener::add_hook(EventListener::KeyBoardKey keystroke,
                        EventType type, std::function<void()> callback) {

  if(type == EventType::repeat
     && !_repeat_triggers[keystroke])
      _repeat_triggers[keystroke] = callback;
  else if(type == EventType::doItOnce
     && !_doitonce_triggers[keystroke]) {
      _doitonce_triggers[keystroke] = callback;
  }
}

void
EventListener::add_hook(MouseKey key,  std::function<void()> callback) {
    if(!_mouse_triggers[key])
      _mouse_triggers[key] = callback;
}

void
EventListener::capture(void) {
    _keyboard->capture();
    _mouse->capture();
}

void
EventListener::check_events(void) {
  if(_mouse_key.first != OIS::MB_Button7)
    trigger_mouse_events();

  if(!_events.empty())
    trigger_keyboard_events();
}

void
EventListener::trigger_mouse_events() {
  if(_mouse_triggers[_mouse_key]) {
    _mouse_triggers[_mouse_key]();
    _mouse_key =  {OIS::MB_Button7, EventTrigger::OnKeyReleased};
  }
}

void
EventListener::trigger_keyboard_events() {
  if(_events.size() > _doitonce_triggers.size() + _repeat_triggers.size())
    return;

  for(auto event: _events){
    if(_doitonce_triggers[event]){
      _doitonce_triggers[event]();
      remove_key_from_buffer(event);
    }

    if(_repeat_triggers[event]) {
      _repeat_triggers[event]();
    }
  }
}

bool
EventListener::shutdown(void) {
    _exit = true;
    return true;
}

void
EventListener::clear_hooks() {
  _mouse_triggers.clear();
  _repeat_triggers.clear();
  _doitonce_triggers.clear();
}

bool
EventListener::keyPressed(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyDown((CEGUI::Key::Scan) arg.key);
  context.injectChar(arg.text);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyReleased});
  _events.push_back({arg.key, EventTrigger::OnKeyPressed});
  return true;
}

bool
EventListener::keyReleased(const OIS::KeyEvent& arg) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().
    getDefaultGUIContext();
  context.injectKeyUp((CEGUI::Key::Scan)arg.key);

  remove_key_from_buffer({arg.key, EventTrigger::OnKeyPressed});
  _events.push_back({arg.key, EventTrigger::OnKeyReleased});
   return true;
}

bool
EventListener::mouseMoved(const OIS::MouseEvent& evt) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
  return true;
  }
bool
EventListener::mousePressed(const OIS::MouseEvent& evt,
                            OIS::MouseButtonID id) {
      CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
    context.injectMouseButtonDown(convertMouseButton(id));
  _x = evt.state.X.abs;
  _y = evt.state.Y.abs;
  _mouse_key = {id, EventTrigger::OnKeyPressed};
  return true;
}

bool
EventListener::mouseReleased(const OIS::MouseEvent& evt,
                             OIS::MouseButtonID id) {
  CEGUI::GUIContext& context = CEGUI::System::getSingleton().getDefaultGUIContext();
  context.injectMouseButtonUp(convertMouseButton(id));
  return true;
}

void
EventListener::windowClosed(Ogre::RenderWindow* window){
  _exit = true;
}

void
EventListener::create_input_manager(Ogre::RenderWindow* window) {
  typedef std::pair<std::string, std::string> parameter;
    OIS::ParamList parameters;
    size_t xid = 0;

    window->getCustomAttribute("WINDOW", &xid);
    parameters.insert(parameter("WINDOW", std::to_string(xid)));
    parameters.insert(parameter("x11_mouse_grab", "false"));
    parameters.insert(parameter("x11_mouse_hide", "false"));
    parameters.insert(parameter("x11_keyboard_grab", "false"));
    parameters.insert(parameter("XAutoRepeatOn", "false"));

    _input_manager = OIS::InputManager::createInputSystem(parameters);
}

void
EventListener::remove_key_from_buffer(KeyBoardKey event) {
    auto keyevent = find (_events.begin(), _events.end(), event);
    if(keyevent == _events.end())
      return;
    _events.erase(keyevent);
}

CEGUI::MouseButton
EventListener::convertMouseButton(OIS::MouseButtonID id) {
  CEGUI::MouseButton cegui_id;
  switch(id)
    {
    case OIS::MB_Left:
      cegui_id = CEGUI::LeftButton;
      break;
    case OIS::MB_Right:
      cegui_id = CEGUI::RightButton;
      break;
    case OIS::MB_Middle:
      cegui_id = CEGUI::MiddleButton;
      break;
    default:
      cegui_id = CEGUI::LeftButton;
    }
  return cegui_id;
}

bool
EventListener::gui_shutdown(const CEGUI::EventArgs &event) {
    _exit = true;
    return true;
}