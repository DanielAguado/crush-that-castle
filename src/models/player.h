#ifndef PLAYER_HPP
#define PLAYER_HPP
#include <sstream>
#include <memory>
#include <iomanip>

#include "sound.h"

class Player {
public:
  typedef std::shared_ptr<Player> shared;

  int _level;
  int _score;
  int _balls;

  Player(Sound::shared sound);
  ~Player();

  void reset();

  bool has_lose();
  bool has_win();

  int get_score();

  void clear_counters();
  float get_time_counter();

  void next_level();
  void increase_time(float deltaT);
  void increase_score();

  void reload_ball();
  int get_balls_number();

  std::string to_string();
  void pick_money();

 private:
  bool _cannon_unloaded;
  float _counter, _reload_delay;
  Sound::shared _sound;
  float _time_counter;

  std::string to_string_with_precision(const float number, const int n);
};

#endif
