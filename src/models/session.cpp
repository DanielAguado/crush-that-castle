#include "session.h"
#include "game.h"

Session::Session(Game::shared game) {
  _game = game;
  _ball = std::make_shared<Ball>();
  _physics = std::make_shared<Physics>();
  _cannon = std::make_shared<Cannon>();
  _camera_index = 1;
  _started = false;
  _finished = false;
  _next_scenario = false;
}

Session::~Session() {
  _game->_input->clear_hooks();
  _game->_scene->destroy_scene();
  _game->_animation->clear();
}

bool
Session::start(const CEGUI::EventArgs &event){
  initialize_overlays();
  _player->clear_counters();
  _started = true;
  _finished = false;
  show_interface();

  return true;
}

void
Session::initialize( Player::shared player) {
  _player = player;

  initialize_cannon();
  initialize_ball();
  initialize_scenario();
  initialize_cameras();
  initialize_interface();
  add_collision_hooks();
}

void
Session::next_scenario() {
  _next_scenario = false;
  _game->_player->reset();
  _game->_player->_level = (_player->_level + 1) % 4;
  _game->_animation->clear();
  _scenario->clean();
  std::stringstream level;
  level << "media/scenarios/level" << _player->_level << ".txt";
  _scenario->build_next(level.str());
}

void
Session::update(float deltaT) {
  if(!_started)
    return;

  _ball->update();
  _scenario->update(_player);
  _physics->step_simulation(deltaT, 2048);

  _physics->check_collision();
  _game->_animation->update(deltaT);
  _player->increase_time(deltaT);

  update_overlays();

  if(_scenario->_finished || _player->has_lose()) {
    //_game->_sound->play_fx("media/sound/win.wav");
    reset_position();
    _next_scenario = true;
    _finished = true;
  }
}

void
Session::update_overlays(){
  if (!_started)
    return;

  _timer_overlay->
    setCaption("Time: " + to_string_with_precision(_player->get_time_counter(), 3));

  _balls_overlay->setCaption(std::to_string(_player->_balls));

  _score_overlay->setCaption(std::to_string(_player->_score));
}

void
Session::clean_session() {
  reset_overlays();
}

void
Session::reset_overlays(){
  _timer_overlay->hide();
  _balls_overlay->hide();
  _score_overlay->hide();

  _timer_overlay->setCaption("");
  _balls_overlay->setCaption("");
  _score_overlay->setCaption("");
}

void
Session::show_overlays() {
  if(!_started)
    return;

  _timer_overlay->show();
  _balls_overlay->show();
  _score_overlay->show();
}

std::string
Session::to_string_with_precision(const float number, const int n) {
  std::ostringstream number_stringify;
  number_stringify << std::setprecision(n) << number;
  return number_stringify.str();
}

void
Session::initialize_overlays() {
  _timer_overlay = _game->_gui->create_overlay("Info", "Timer");
  _balls_overlay = _game->_gui->create_overlay("Info", "Balls");
  _score_overlay = _game->_gui->create_overlay("Info", "Score");
}

void
Session::initialize_scenario() {
  std::stringstream level;
  level << "media/scenarios/level" << _player->_level << ".txt";
  _scenario = std::make_shared<Scenario>(_game->_scene, _physics,
                   _game->_sound, _game->_animation, level.str());
}

void
Session::initialize_ball() {
  btVector3 origin = btVector3(-107, 4.5, 0);
  _ball->initialize(_game->_scene, _physics,  _game->_gui, _game->_sound,
                     _game->_gui->load_layout("Session.layout"), origin);
}

void
Session::initialize_cannon() {
  btVector3 origin = btVector3(-110, 0, 0);
  _cannon->initialize(_game->_scene, _physics, _game->_sound, origin);
}

void
Session::add_collision_hooks() {
  _scenario->add_collision_hooks(_ball->_body, std::bind(&Ball::collide, _ball.get()));
}

void
Session::show_interface() {
  show_overlays();
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
  context_.setRootWindow(_ball->_power_bar);
  _ball->_power_bar->setVisible(true);
}

void
Session::reset_position() {
  _cannon->reset();
  _ball->reset();
  _player->reload_ball();
}

void
Session::initialize_cameras() {
  _game->_scene->create_camera("camera_ball", Ogre::Vector3(-30, 40, 70),
              Ogre::Vector3(-10, -10, -30));

   Ogre::SceneNode* camera_node = _game->_scene->get_node("camera_ball");
  _game->_scene->get_node("ball")->addChild(camera_node);
}

void
Session::set_camera() {
  _game->_scene->switch_camera((_camera_index++) % 2);
}

void
Session::register_keybindings(){
  _game->_input->add_hook({OIS::KC_E, EventTrigger::OnKeyPressed},
                          EventType::repeat,
                          std::bind(&Ball::charge, _ball.get()));

  _game->_input->add_hook({OIS::KC_E, EventTrigger::OnKeyReleased},
                          EventType::doItOnce,
                          std::bind(&Ball::apply_force, _ball.get()));

  _game->_input->add_hook({OIS::KC_R, EventTrigger::OnKeyReleased},
                          EventType::doItOnce,
                          std::bind(&Session::reset_position, this));

  _game->_input->add_hook({OIS::KC_W, EventTrigger::OnKeyPressed},
                          EventType::repeat,
                          std::bind(&Cannon::rotate, _cannon.get(), 1));

  _game->_input->add_hook({OIS::KC_S, EventTrigger::OnKeyPressed},
                          EventType::repeat,
                          std::bind(&Cannon::rotate, _cannon.get(), -1));
  _game->_input->add_hook({OIS::KC_9, EventTrigger::OnKeyPressed},
                          EventType::doItOnce,
                          std::bind(&Session::set_camera, this));
}

void
Session::initialize_interface() {
  _intro_window = _game->_gui->load_layout("Intro.layout");
  CEGUI::GUIContext& context_ = CEGUI::System::getSingleton().getDefaultGUIContext();
    context_.setRootWindow(_intro_window);
  add_gui_hooks();
}

void
Session::add_hooks(CEGUI::Window *window,
                const std::string& button,
                 const CEGUI::Event::Subscriber& callback) {
  window->getChild(button)->
    subscribeEvent(CEGUI::PushButton::EventClicked, callback);
}

void
Session::add_gui_hooks() {
  add_hooks(_intro_window, "Ok",
            CEGUI::Event::Subscriber(&Session::start, this));

}
