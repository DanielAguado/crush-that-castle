#include "ball.h"

Ball::Ball(): _max_power(17000000), _power_step(170000){
  _armed = true;
  _power = 0;
  _collide = true;
}

Ball::~Ball(){

}

void
Ball::initialize(Scene::shared scene, Physics::shared physics, GUI::shared gui, Sound::shared sound, CEGUI::Window* window, btVector3 origin) {
  _physics = physics;
  _gui = gui;
  _sound = sound;
  _power_bar = window;
  _origin = origin;
  _scene = scene;

  Ogre::Entity* entity =  _scene->create_entity("ball_ent", "Sphere.mesh");
  Ogre::SceneNode* node = _scene->create_graphic_element(entity, "", "ball");

  btCollisionShape* shape = physics->create_shape(1);

  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);

  int mass = 1000;
  _body = physics->create_rigid_body(transformation, node, shape, mass);

  _body->setRestitution(1.5);
  _body->setLinearFactor(btVector3(1, 1, 0));
  _body->setAngularFactor(btVector3(0,0,0));

  _scene->create_particle(_scene->get_child_node(node, "ball_particle"),
                                 "ball_particle", "Bomb/ProjSmoke");
}

void
Ball::reset() {
  _armed = true;
  _physics->move(_body, _origin);
  static_cast<CEGUI::ProgressBar*>( _power_bar->getChild("PowerBar"))->
    adjustProgress(-1.f);
}

void
Ball::update() {
  if(_armed)
    _body->setLinearVelocity(btVector3(0,0,0));
}

void
Ball::apply_force() {
  if(_armed)
    _body->applyCentralForce(btVector3(_power, 5, 0));
  _power = 0;
  _armed = false;
  _collide = true;
  _sound->play_fx("media/sound/cannon_shoot.mp3");
}

void
Ball::charge() {
  CEGUI::ProgressBar* progress =
    static_cast<CEGUI::ProgressBar*>( _power_bar->getChild("PowerBar"));
  progress->step();
  _power = (_power > _max_power)? _max_power : _power + _power_step;
}

void
Ball::collide() {
  if (!_collide)
    return;
    Ogre::Vector3 position = _scene->convert_btvector3_to_vector3(_body->getCenterOfMassPosition());
    std::stringstream name;
    name << position.x << position.y << position.z << "particle";
    _scene->create_particle(name.str(), "Bomb/Explosion", position);
    _collide = false;
    _sound->play_fx("media/sound/explosion.wav");
}
