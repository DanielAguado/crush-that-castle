#ifndef CANNON_HPP
#define CANNON_HPP
#include <memory>
#include <cmath>

#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "physics.h"
#include "sound.h"
#include "scene.h"

class Cannon {

  Sound::shared _sound;
  Scene::shared _scene;
  Physics::shared _physics;

  btRigidBody* _body;

 public:
  typedef std::shared_ptr<Cannon> shared;

  Cannon();
  ~Cannon();

  void initialize(Scene::shared scene, Physics::shared physics,
                  Sound::shared sound, btVector3 origin);
  void reset();
  void rotate(int angle);
};
#endif
