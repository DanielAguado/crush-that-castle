#include "player.h"
#include <iostream>

Player::Player(Sound::shared sound) {
  _sound = sound;
  _level = 0;
  reset();
}

Player::~Player(){

}

void
Player::reset(){
  _score = 0;
  _counter = _time_counter = 0.0;
  _balls = 5;
  _reload_delay = 1.f;
}

void
Player::clear_counters(){
	_time_counter = 0.0;
	_score = 0;
}

float
Player::get_time_counter(){
	return _time_counter;
}

bool
Player::has_lose(){
  return _balls <= 0;
}

bool
Player::has_win() {
  return false;
}

void
Player::increase_time(float deltaT){
	_time_counter += deltaT;
  _counter += deltaT;

  if(_counter >= _reload_delay ) {
    _cannon_unloaded = true;
    _counter = 0;
  }
}


void
Player::reload_ball() {
  if(_cannon_unloaded) {
    _cannon_unloaded = false;
    _counter = 0;
    _balls--;
  }
}

std::string
Player::to_string() {
  std::stringstream string;
  string << "Level: " << _level
         << " Time: " << to_string_with_precision(_time_counter, 4)
         << " Score: " << _score
         << " Balls: " << _balls << "\n";

  return string.str();

}

std::string
Player::to_string_with_precision(const float number, const int n) {
    std::ostringstream number_stringify;
    number_stringify << std::setprecision(n) << number;
    return number_stringify.str();
}

void
Player::pick_money() {
  _score++;
}
