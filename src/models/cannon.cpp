#include "cannon.h"

Cannon::Cannon(){

}

Cannon::~Cannon(){

}

void
Cannon::initialize(Scene::shared scene, Physics::shared physics,
                   Sound::shared sound, btVector3 origin) {
  _physics = physics;
  Ogre::Entity* entity_cannon =
    scene->create_entity("cannon_ent", "cannon.mesh");
  Ogre::SceneNode* node_cannon =
    scene->create_graphic_element(entity_cannon, "", "cannon");

  Ogre::Entity* entity_cannon_structure =
    scene->create_entity("cannon_structure_ent", "cannon_structure.mesh");
  Ogre::SceneNode* node_cannon_structure = scene->create_graphic_element(
                          entity_cannon_structure, "", "cannon_structure");
  node_cannon_structure->setPosition(Ogre::Vector3(origin.x(),
                                                   origin.y(),
                                                   origin.z()));

  btCollisionShape* shape = _physics->create_shape(
        new MeshStrider(entity_cannon->getMesh().get()));

  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation(rotation, origin);
  int mass = 100000;
  _body = _physics->create_rigid_body(transformation, node_cannon, shape, mass);
}

void
Cannon::rotate(int angle){
  float radians_conversion = 0.0174532925;

  float radians = angle * radians_conversion;
  radians += _body->getOrientation().getAngle();

  float degrees = radians/radians_conversion;
  if (degrees > 45 || degrees < 0)
    return;

  btQuaternion rotation(btVector3(0, 0, 1), btScalar(radians));

  btTransform transformation(rotation, _body->getCenterOfMassPosition());
  _body->setCenterOfMassTransform(transformation);
}

void
Cannon::reset(){
  btQuaternion rotation(btVector3(0, 0, 1), btScalar(0));
  btTransform transformation(rotation, _body->getCenterOfMassPosition());
  _body->setCenterOfMassTransform(transformation);
}
