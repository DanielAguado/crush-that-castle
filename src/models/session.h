#ifndef SESSION_HPP
#define SESSION_HPP
#include <memory>
#include <sstream>

#include "physics.h"
#include "meshstrider.h"
#include "player.h"
#include "ball.h"
#include "cannon.h"
#include "scenario.h"

class Game;

class Session {
  bool _started;
  int _camera_index;
  Scenario::shared _scenario;
  std::shared_ptr<Game> _game;
  Physics::shared _physics;
  CEGUI::Window *_intro_window;

  Player::shared _player;
  Ogre::OverlayElement* _timer_overlay;
  Ogre::OverlayElement* _balls_overlay;
  Ogre::OverlayElement* _score_overlay;

 public:
  typedef std::shared_ptr<Session> shared;
  bool _finished;
  bool _next_scenario;
  Ball::shared _ball;
  Cannon::shared _cannon;

  Session(std::shared_ptr<Game> game);
  ~Session();

  void initialize(Player::shared player);
  void update(float deltaT);
  void update_overlays();
  void show_interface();
  void show_overlays();

  void clean_session();
  void reset_overlays();
  void reset_position();
  void next_scenario();

  void register_keybindings();

 private:
  bool start(const CEGUI::EventArgs &event);

  void initialize_ground();
  void initialize_cannon();
  void initialize_ball();
  void initialize_scenario();
  void initialize_cameras();
  void initialize_interface();
  void initialize_overlays();

  void add_collision_hooks();
  void set_camera();
  void add_gui_hooks();
  void add_hooks(CEGUI::Window *window,
                 const std::string& button,
                 const CEGUI::Event::Subscriber& callback);

  std::string to_string_with_precision(const float number, const int n);
};
#endif
