#include "scenario.h"
Scenario::Scenario() {

}

Scenario::Scenario(Scene::shared scene, Physics::shared physics,
                   Sound::shared sound, Animation::shared animation,
                   std::string scenario_filename){
  _x = _y = 0;
  _finished = false;
  _number_destroyable_objects = 0;
	_scenario_filename = scenario_filename;
	_scene = scene;
	_physics = physics;
  _animation = animation;
  _sound = sound;

	_builder['|'] = std::bind(&Scenario::create_piece, this, "wall.mesh");
	_builder['_'] = std::bind(&Scenario::create_piece, this, "floor.mesh");
	_builder['\\'] = std::bind(&Scenario::create_piece, this, "roof.mesh");
  _builder['o'] = std::bind(&Scenario::create_piece, this, "bag.mesh");

	load();
  add_default_scenery();
}

Scenario::~Scenario() {
}

void
Scenario::clean() {
  if(_scenario_nodes.empty())
    return;
   for(auto node: _scenario_nodes)
    _scene->destroy_node(node);

  _scenario_nodes.clear();
  for(auto physic_body: _scenario_bodies) {
    _physics->remove_rigid_body(physic_body.first);
    delete physic_body.first;
    delete physic_body.second;
  }
  _scenario_bodies.clear();
  _money.clear();
  _x = _y = 0;
  _finished = false;
  _number_destroyable_objects = 0;
}

void
Scenario::add_collision_hooks(btRigidBody* body, std::function<void()> callback) {
  for (auto piece: _scenario_bodies)
    _physics->add_collision_hooks(Physics::CollisionPair{body, piece.first}, callback);

}

void
Scenario::build_next(std::string file) {
	_scenario_filename = file;
  load();
}

void
Scenario::load() {
	read_file();
	parse();
  _sound->play_fx("media/sound/forest_sound.wav");
}

void
Scenario::open_file() {
  std::cout << _scenario_filename << std::endl;
  _file.open(_scenario_filename);
  if (!_file.is_open()) {
    std::cerr << "no file: \n"
              << _scenario_filename
              << std::endl;
    exit(-1);
  }
}

void
Scenario::read_file(){
	open_file();

	for (std::string line; getline(_file, line);)
		_scenario.push(line);

  _file.close();
}

void
Scenario::create_piece(std::string mesh) {
	int offset = 10;
  int pos_x = (mesh == "roof.mesh")?
    (_x - 1) * offset : _x * offset;
  int pos_y = _y * offset;

  if (mesh == "bag.mesh"){
    pos_y = _y * (offset / 2);
    _number_destroyable_objects++;
  }

	std::stringstream name;
	name << mesh << _x << _y;
  Ogre::SceneNode* node =
    _scene->create_graphic_element(name.str(), mesh, "", name.str());
  _scenario_nodes.push_back(node);

  int mass = 1000;
  btVector3 origin = btVector3(pos_x, pos_y, 0);
  btQuaternion rotation(btVector3(0, 1, 0), btScalar(0));
  btTransform transformation = btTransform(rotation, origin);
  btCollisionShape* shape = build_shape(mesh, node);
  btRigidBody* body = _physics->create_rigid_body(transformation, node, shape, mass);
  body ->setLinearFactor(btVector3(1, 1, 0));
  _scenario_bodies.push_back({body, shape});

  if (mesh == "bag.mesh"){
    _money.push_back({node, body});
    _animation->activate(
             static_cast<Ogre::Entity*>(node->getAttachedObject(0)),
             "spining", true);
  }
}

void
Scenario::parse() {
 	std::string tier;

	while(!_scenario.empty()){
		tier = _scenario.top();
		_scenario.pop();

		_y++;
		_x = 0;

		for(auto brick: tier){
			_x++;
      if(_builder[brick])
        _builder[brick]();
		}
	}
}

btCollisionShape*
Scenario::build_shape(std::string mesh, Ogre::SceneNode* node) {
  if (mesh == "wall.mesh")
  	return _physics->create_shape(btVector3(2.5, 10, 10));

  if (mesh == "roof.mesh")
    return _physics->create_shape(20, 20);

  if (mesh == "floor.mesh")
    return _physics->create_shape(btVector3(10, 1.5, 10));

  if (mesh == "bag.mesh")
    return _physics->create_shape(3.5);

  return nullptr;
}

void
Scenario::update(Player::shared player) {
  for(auto info: _money) {
    if(info.second->getLinearVelocity().x() > 0.3f) {
      std::stringstream name;
      name << "powerup_particle" + info.first->getName();
      _scene->create_particle(name.str(), "Bomb/WickEmitter",
                              info.first->getPosition());
      _scene->remove_child("", info.first);
      info.second->setLinearVelocity(btVector3(0, 0, 0));
      _physics->remove_rigid_body(info.second);
      player->pick_money();
      _number_destroyable_objects--;
    }
  }

  if(_number_destroyable_objects <= 0)
    _finished = true;
}

void
Scenario::add_default_scenery() {
  Ogre::SceneNode* ground_node =
    _scene->create_plane("Y", "ground", "Ground", "", "Ground");

  btCollisionShape* groundShape =
    _physics->create_shape(btVector3(400, 1, 400));
   btRigidBody* plane_body =
     _physics->create_rigid_body(btTransform(btQuaternion(0, 0, 0, 1),
                         btVector3(0, 1, 0)),ground_node, groundShape, 0);
  plane_body->setRestitution(0.2);
  plane_body->setFriction(0.5f);

  add_entity("mountain1", "mountain.mesh", Ogre::Vector3(50, 1.1, -100));
  add_entity("mountain2", "mountain.mesh", Ogre::Vector3(-100, 1.1, -200));
  add_entity("mountain3", "mountain.mesh", Ogre::Vector3(300, 1.1, -100));
  add_entity("mountain4", "mountain.mesh", Ogre::Vector3(-50, 1.1, -300));

  add_entity("tree1", "tree1.mesh", Ogre::Vector3(-55, 1.1, -150), .5f);
  add_entity("tree2", "tree1.mesh", Ogre::Vector3(-60, 1.1, -150), .5f);
  add_entity("tree3", "tree1.mesh", Ogre::Vector3(-10, 1.1, -80), .5f);
  add_entity("tree4", "tree1.mesh", Ogre::Vector3(0, 1.1, -100), .5f);
  add_entity("tree5", "tree1.mesh", Ogre::Vector3(-20, 1.1, -90), .5f);
  add_entity("tree6", "tree1.mesh", Ogre::Vector3(-30, 1.1, -70), .5f);
  add_entity("tree7", "tree1.mesh", Ogre::Vector3(-100, 1.1, -100), .5f);
  add_entity("tree8", "tree1.mesh", Ogre::Vector3(-110, 1.1, -90), .5f);
  add_entity("tree9", "tree1.mesh", Ogre::Vector3(-90, 1.1, -70), .5f);
  add_entity("tree10", "tree1.mesh", Ogre::Vector3(180, 1.1, -100), .5f);
  add_entity("tree11", "tree1.mesh", Ogre::Vector3(170, 1.1, -90), .5f);
  add_entity("tree12", "tree1.mesh", Ogre::Vector3(120, 1.1, -80), .5f);
  add_entity("tree13", "tree1.mesh", Ogre::Vector3(120, 1.1, -90), .5f);
  add_entity("tree14", "tree1.mesh", Ogre::Vector3(130, 1.1, -80), .5f);
}

void
Scenario::add_entity(std::string name, std::string mesh,
                     Ogre::Vector3 position, float scale) {
   Ogre::Entity* entity =
     _scene->create_entity(name, mesh, false);
  Ogre::SceneNode* node =
    _scene->create_graphic_element(entity, "", name);
  node->setPosition(position);
  node->setScale(Ogre::Vector3(scale, scale, scale));
}
