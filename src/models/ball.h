#ifndef BALL_
#define BALL_HPP
#include <memory>
#include <cmath>

#include <OgreEntity.h>
#include <OgreSceneNode.h>

#include "physics.h"
#include "sound.h"
#include "scene.h"
#include "gui.h"

class Ball {
  Physics::shared _physics;
  GUI::shared _gui;
  Sound::shared _sound;
  Scene::shared _scene;

  bool _armed;
  int _power;
  bool _collide;
  const int _max_power, _power_step;
  btVector3 _origin;

 public:
  typedef std::shared_ptr<Ball> shared;
  CEGUI::Window* _power_bar;
  btRigidBody* _body;

  Ball();
  ~Ball();

  void initialize(Scene::shared scene, Physics::shared physics,
                  GUI::shared gui, Sound::shared sound,
                  CEGUI::Window* window, btVector3 origin);
  void reset();
  void charge();
  void apply_force();
  void update();

  btVector3 get_direction();
  void collide();

private:
  float correct_direction(float direction, float correction);
};
#endif
