#ifndef SCENARIO_H
#define SCENARIO_H

#include <iostream>
#include <fstream>
#include <string>
#include <stack>
#include <map>
#include <functional>

#include "physics.h"
#include "scene.h"
#include "animation.h"
#include "meshstrider.h"
#include "player.h"

class Scenario {
  int _x, _y;
  int _number_destroyable_objects;
	std::string _scenario_filename;
	std::ifstream _file;
  std::vector<Ogre::SceneNode*> _scenario_nodes;
  std::vector<std::pair<btRigidBody*, btCollisionShape*>> _scenario_bodies;

	std::stack<std::string> _scenario;
	std::map<char, std::function<void()>> _builder;
	Scene::shared _scene;
  Physics::shared _physics;
	Animation::shared _animation;
  Sound::shared _sound;

 public:
  typedef std::shared_ptr<Scenario> shared;

  std::vector<std::pair<Ogre::SceneNode*, btRigidBody*>> _money;
  bool _finished;

  Scenario();
  Scenario(Scene::shared scene, Physics::shared physics,
           Sound::shared sound, Animation::shared animation,
           std::string scenario_filename);
  ~Scenario();

  void load();
  void update(Player::shared player);
  void clean();
  void build_next(std::string file);
  void add_collision_hooks(btRigidBody* body, std::function<void()> callback);

  bool scenenario_finished();

 private:
	void open_file();
	void read_file();
	void parse();
	void create_piece(std::string mesh);
  btCollisionShape* build_shape(std::string mesh, Ogre::SceneNode* node);
  void add_default_scenery();
  void add_entity(std::string name, std::string mesh,
                  Ogre::Vector3 position, float scale = 1.f);
};
#endif
