#include "timer.h"
#include <iostream>

Timer::Timer() {
  last_time_ = std::chrono::steady_clock::now();
  start_ = 0.f;
  stop_ = now();
}

Timer::~Timer() { }

float
Timer::get_delta_time() {
  delta_ = now() - last_time_;
  last_time_ = now();

  return time_to_float(delta_);
}

float
Timer::get_time_since_start() {
  start_ += get_delta_time();

  return truncate(start_);
}

void
Timer::start() {
  freeze_time_ = now() - stop_;
  start_ -= time_to_float(freeze_time_);
}

void
Timer::stop() {
  stop_ = now();
}

float
Timer::truncate(float number) {
  return std::floor(number * 10) / 10;
}

Timer::Time
Timer::now() {
  return std::chrono::steady_clock::now();
}

float
Timer::time_to_float(Timer::DeltaTime time) {
  return std::chrono::duration_cast<std::chrono::duration<float>>(time).count();
}