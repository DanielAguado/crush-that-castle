#include "camera_slider.h"
#include "game.h"

void
CameraSlider::register_keybindings(Game::shared _game) {
  _game->_input->add_hook({OIS::KC_R, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(0, 0.25, 0)));

  _game->_input->add_hook({OIS::KC_F, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(0, -0.25, 0)));

  _game->_input->add_hook({OIS::KC_A, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(-0.25, 0, 0)));

  _game->_input->add_hook({OIS::KC_D, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(0.25, 0, 0)));

  _game->_input->add_hook({OIS::KC_W, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(0, 0, -0.25)));

  _game->_input->add_hook({OIS::KC_S, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::move_camera, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Vector3(0, 0, 0.25)));

  _game->_input->add_hook({OIS::KC_U, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_x, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(0.5)));

  _game->_input->add_hook({OIS::KC_I, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_x, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(-0.5)));

  _game->_input->add_hook({OIS::KC_J, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_y, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(0.5)));

  _game->_input->add_hook({OIS::KC_K, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_y, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(-0.5)));

  _game->_input->add_hook({OIS::KC_N, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_z, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(0.5)));

  _game->_input->add_hook({OIS::KC_M, EventTrigger::OnKeyPressed}, EventType::repeat,
                                std::bind(&CameraSlider::rotate_camera_z, 
                                _game->_scene->get_node("camera_node"), 
                                Ogre::Degree(-0.5)));
}

void
CameraSlider::unregister_callbacks(Game::shared game) {
  game->_input->clear_hooks();
}

void
CameraSlider::move_camera(Ogre::SceneNode* camera_node, Ogre::Vector3 increment) {
	Ogre::Camera* camera = static_cast<Ogre::Camera*>(
													camera_node->getAttachedObject(0));

	Ogre::Vector3 position = camera->getPosition();
  camera->setPosition(position + increment);

  std::cout << "New position " << (position + increment) << std::endl;
}

void
CameraSlider::rotate_camera_x(Ogre::SceneNode* camera, Ogre::Degree degree){
	camera->pitch(degree);
}

void
CameraSlider::rotate_camera_y(Ogre::SceneNode* camera, Ogre::Degree degree){
	camera->yaw(degree);
}

void
CameraSlider::rotate_camera_z(Ogre::SceneNode* camera, Ogre::Degree degree){
	camera->roll(degree);
}
